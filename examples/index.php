<?php
include 'inc.php';

$api = new \SolucionTotal\APIGit\API("user","password");

$repos = $api->getRepos();
$usuario = $api->getCurrentUser();
//sdfasdfsdaf
echo '<b>Repos del usuario '.$usuario->login.'</b></br>';
foreach($repos as $repo){
    echo $repo->name.'</br>';
}
echo '<hr>';
$issues = $api->getIssues('lgsilvestre', 'PataconAPP_G3_patriarcas', 2, 2);
echo '<b>Issues del repositorio PataconAPP_G3_patriarcas</b></br>';
foreach($issues as $issue){
    echo '['.$issue->number.'] '.$issue->title.' &lt;'.$issue->user->login.'&gt;</br>';
}
echo '<hr>';
echo '<b>Comentarios del Issue 25 en repositorio PataconAPP_G3_patriarcas</b></br>';
$comentarios = $api->getIssueComments('lgsilvestre', 'PataconAPP_G3_patriarcas', 25);

foreach($comentarios as $comentario){
    echo '<b>Autor:</b> '.$comentario->user->login.'</br>';
    echo '<b>Cuerpo:</b> </br>'.$comentario->body.'</br>';
}
echo '<hr>';
echo '<b>Commits repositorio PataconAPP_G3_patriarcas</b></br>';
$commits = $api->getCommits('lgsilvestre', 'PataconAPP_G3_patriarcas');

foreach($commits as $commit){
    echo '<b>SHA:</b> '.$commit->sha.'</br>';
    echo '<b>Autor:</b> '.$commit->commit->author->name.'</br>';
}
echo '<hr>';
echo '<b>Branches repositorio PataconAPP_G3_patriarcas</b></br>';
$branches = $api->getBranches('lgsilvestre', 'PataconAPP_G3_patriarcas');

foreach($branches as $branch){
    echo '<b>Nombre:</b> '.$branch->name.'</br>';
    echo '<b>SHA:</b> '.$branch->commit->sha.'</br>';
}
echo '<hr>';
echo '<b>Commits repositorio PataconAPP_G3_patriarcas desde branch feature/AddCssLogin</b></br>';
$commits = $api->getCommits('lgsilvestre', 'PataconAPP_G3_patriarcas', 'feature/AddCssLogin');

foreach($commits as $commit){
    echo '<b>SHA:</b> '.$commit->sha.'</br>';
    echo '<b>Autor:</b> '.$commit->commit->author->name.'</br>';
}
echo '<hr>';
echo '<b>Commit 1 repositorio PataconAPP_G3_patriarcas</b></br>';

$commit1 = $api->getCommit('lgsilvestre', 'PataconAPP_G3_patriarcas', 'e58cb4610d86efecf443f1f74b6cc3e575b965c8');
echo '<b>SHA:</b> '.$commit1->sha.'</br>';
echo '<b>Autor:</b> '.$commit->commit->author->name.'</br>';
echo '<b>Estadisticas</b></br>';
echo '<b>Total:</b> '.$commit1->stats->total.'</br>';
echo '<b>Agregan:</b> '.$commit1->stats->additions.'</br>';
echo '<b>Eliminan:</b> '.$commit1->stats->deletions.'</br>';

echo '<hr>';
echo '<b>Registro de eventos PataconAPP_G3_patriarcas</b></br>';

$eventos = $api->getEventsLog('lgsilvestre', 'PataconAPP_G3_patriarcas');
foreach($eventos as $evento){
    if($evento->type == 'IssuesEvent'){
        echo '<b>Evento del tipo IssuesEvent</b></br>';
        echo '<b>Accion:</b> '.$evento->payload->action.'</br>';
        echo '<b>Numero:</b> '.$evento->payload->issue->number.'</br>';
        echo '<b>Titulo:</b> '.$evento->payload->issue->title.'</br>';
    }else if($evento->type == 'IssueCommentEvent'){
        echo '<b>Evento del tipo IssueCommentEvent</b></br>';
        echo '<b>Accion:</b> '.$evento->payload->action.'</br>';
        echo '<b>Numero:</b> '.$evento->payload->issue->number.'</br>';
        echo '<b>Titulo:</b> '.$evento->payload->issue->title.'</br>';
        echo '<b>Comentario:</b> '.$evento->payload->comment->body.'</br>';
    }
}
?>