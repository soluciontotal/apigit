<?php

namespace SolucionTotal\APIGit;

class API {
    
    public CONST ISSUES_OPEN = 0;
    public CONST ISSUES_CLOSED = 1;
    public CONST ISSUES_ALL = 2;

    private $username;
    private $password;
    private $currentUser;

    /**
     * Constructor de la clase, recibe el usuario y password de github.
     */
    function __construct($username, $password){
        $this->username = $username;
        $this->password = $password;
        $this->currentUser = $this->getUser();

    }
    /**
     * Metodo para obtener el usuario actual
     */
    public function getCurrentUser(){
        return $this->currentUser;
    }

    /**
     * Metodo para obtener los datos del usuario conectado.
     */
    private function getUser(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,'https://api.github.com/user');
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.3 Safari/605.1.15');
        curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $respuesta = curl_exec ($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close ($ch);
        if($http_status == 200){
            return json_decode($respuesta);
        }else if($http_status == 401){
            throw new \Exception('Usuario no autorizado para acceder a este repositorio.');
        }else{
            throw new \Exception('Error en la libreria.');
        }
    }
    /**
     * Metodo para obtener todos los respositorios del usuario conectado,
     */
    public function getRepos($pagina=1){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,'https://api.github.com/user/repos?page='.$pagina);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.3 Safari/605.1.15');
        curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $respuesta = curl_exec ($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close ($ch);
        if($http_status == 200){
            return json_decode($respuesta);
        }else if($http_status == 401){
            throw new \Exception('Usuario no autorizado para acceder a este repositorio.');
        }else{
            throw new \Exception('Error en la libreria.');
        }
    }
    /**
     * Metodo para obtener todos los proyectos de un repositorio
     */
    public function getProjects($owner, $repo){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,'https://api.github.com/repos/'.$owner.'/'.$repo.'/projects');
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.3 Safari/605.1.15');
        curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $respuesta = curl_exec ($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close ($ch);
        if($http_status == 200){
            return json_decode($respuesta);
        }else if($http_status == 401){
            throw new \Exception('Usuario no autorizado para acceder a este repositorio.');
        }else{
            throw new \Exception('Error en la libreria.');
        }
    }
    /**
     * Metodo para obtener todas las columnas de un proyecto
     */
    public function getColumns($project_id){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.github.com/projects/'.$project_id.'/columns');
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.3 Safari/605.1.15');
        curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $respuesta = curl_exec ($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close ($ch);
        if($http_status == 200){
            return json_decode($respuesta);
        }else if($http_status == 401){
            throw new \Exception('Usuario no autorizado para acceder a este repositorio.');
        }else{
            throw new \Exception('Error en la libreria.');
        }
    }
    /**
     * Metodo para obtener todas las tarjetas de un proyecto
     */
    public function getCards($column_id){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.github.com/projects/columns/'.$column_id.'/cards');
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.3 Safari/605.1.15');
        curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $respuesta = curl_exec ($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close ($ch);
        if($http_status == 200){
            return json_decode($respuesta);
        }else if($http_status == 401){
            throw new \Exception('Usuario no autorizado para acceder a este repositorio.');
        }else{
            throw new \Exception('Error en la libreria.');
        }
    }    
    /**
     * Metodo para obtener un repositorio en especifico segun owner y repo.
     */
    public function getRepo($owner, $repo){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,'https://api.github.com/repos/'.$owner.'/'.$repo);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.3 Safari/605.1.15');
        curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $respuesta = curl_exec ($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close ($ch);
        if($http_status == 200){
            return json_decode($respuesta);
        }else if($http_status == 401){
            throw new \Exception('Usuario no autorizado para acceder a este repositorio.');
        }else{
            throw new \Exception('Error en la libreria.');
        }
    }
    /**
     * Metodo para obtener los Issues de un repositorio en especifico.
     * Estados: 0 abierta, 1 cerrada, 2 todas
     */
    public function getIssues($owner, $repo, $estado = 0, $pagina = 1){
        $state = '';
        switch($estado){
            case 0:
                $state = 'open';
                break;
            case 1:
                $state = 'closed';
                break;
            case 2:
                $state = 'all';
                break;
            default:
                $state = 'open';
                break;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,'https://api.github.com/repos/'.$owner.'/'.$repo.'/issues?per_page=100&state='.$state.'&page='.$pagina);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.3 Safari/605.1.15');
        curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $respuesta = curl_exec ($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close ($ch);
        if($http_status == 200){
            return json_decode($respuesta);
        }else if($http_status == 401){
            throw new \Exception('Usuario no autorizado para acceder a este repositorio.');
        }else{
            throw new \Exception('Error en la libreria.');
        }
    }
    /**
     * Metodo para obtener el numero de issues de un repositorio en especifico.
     * Estados: 0 abierta, 1 cerrada, 2 todas
     */
    public function getIssuesCount($owner, $repo, $estado = 0, $pagina = 1, $creator = ''){
        $state = '';
        $path_creator = '';
        $issues = 0;
        if($creator != ''){
            $path_creator = '&creator='.$creator;
        }
        switch($estado){
            case 0:
                $state = 'open';
                break;
            case 1:
                $state = 'closed';
                break;
            case 2:
                $state = 'all';
                break;
            default:
                $state = 'open';
                break;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,'https://api.github.com/repos/'.$owner.'/'.$repo.'/issues?per_page=100&state='.$state.'&page='.$pagina.$path_creator);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.3 Safari/605.1.15');
        curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $respuesta = curl_exec ($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close ($ch);
        $data = json_decode($respuesta);
        $issues += count($data);
        $page = 2;
        while(count($data) == 100){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,'https://api.github.com/repos/'.$owner.'/'.$repo.'/issues?per_page=100&state='.$state.'&page='.$page.$path_creator);
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.3 Safari/605.1.15');
            curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $respuesta = curl_exec ($ch);
            $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close ($ch);
            $data = json_decode($respuesta);
            $issues += count($data);
            $page++;
        }
        if($http_status == 200){
            return $issues;
        }else if($http_status == 401){
            throw new \Exception('Usuario no autorizado para acceder a este repositorio.');
        }else{
            throw new \Exception('Error en la libreria.');
        }
    }
    /**
     * Metodo para obtener un Issue especifico de un repositorio.
     */
    public function getIssue($owner, $repo, $number){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,'https://api.github.com/repos/'.$owner.'/'.$repo.'/issues/'.$number);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.3 Safari/605.1.15');
        curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $respuesta = curl_exec ($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close ($ch);
        if($http_status == 200){
            return json_decode($respuesta);
        }else if($http_status == 401){
            throw new \Exception('Usuario no autorizado para acceder a este repositorio.');
        }else{
            throw new \Exception('Error en la libreria.');
        }
    }
    /**
     * Metodo para obtener los comentarios de un Issue especifico.
     */
    public function getIssueComments($owner, $repo, $number, $pagina=1){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,'https://api.github.com/repos/'.$owner.'/'.$repo.'/issues/'.$number.'/comments?page='.$pagina);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.3 Safari/605.1.15');
        curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $respuesta = curl_exec ($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close ($ch);
        if($http_status == 200){
            return json_decode($respuesta);
        }else if($http_status == 401){
            throw new \Exception('Usuario no autorizado para acceder a este repositorio.');
        }else{
            throw new \Exception('Error en la libreria.');
        }
    }
    /**
     * Metodo para obtener los stats de un repositorio.
     */
    public function getStats($owner, $repo, $stat){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,'https://api.github.com/repos/'.$owner.'/'.$repo.'/'.'stats/'.$stat );
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.3 Safari/605.1.15');
        curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $respuesta = curl_exec ($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close ($ch);
        if($http_status == 200){
            return json_decode($respuesta);
        }else if($http_status == 401){
            throw new \Exception('Usuario no autorizado para acceder a este repositorio.');
        }else{
            throw new \Exception('Error en la libreria.');
        }
    }
    /**
     * Metodo para obtener los colaboradores de un repositorio.
     */
    public function getCollaborators($owner, $repo){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,'https://api.github.com/repos/'.$owner.'/'.$repo.'/'.'collaborators' );
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.3 Safari/605.1.15');
        curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $respuesta = curl_exec ($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close ($ch);
        if($http_status == 200){
            return json_decode($respuesta);
        }else if($http_status == 401){
            throw new \Exception('Usuario no autorizado para acceder a este repositorio.');
        }else{
            throw new \Exception('Error en la libreria.');
        }
    }
    /**
     * Metodo para obtener los commits hechos a un repositorio.
     */
    public function getCommits($owner, $repo, $branch = '', $pagina=1, $autor = null, $desde = null, $hasta = null){
        $params_date = '';
        $params_author = '';
        if($desde != null && $hasta != null){
            $params_date = '&since='.date('c', strtotime($desde)).'&until='.date('c', strtotime($hasta));
        }
        if($autor != null){
            $params_author = '&author='.$autor;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,'https://api.github.com/repos/'.$owner.'/'.$repo.'/commits?per_page=100&page='.$pagina.( ($branch=='')?'':('&sha='.$branch) ).$params_date.$params_author);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.3 Safari/605.1.15');
        curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $respuesta = curl_exec ($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close ($ch);
        if($http_status == 200){
            return json_decode($respuesta);
        }else if($http_status == 401){
            throw new \Exception('Usuario no autorizado para acceder a este repositorio.');
        }else{
            throw new \Exception('Error en la libreria.');
        }
    }
    /**
     * Metodo para obtener un commit especifico segun su SHA.
     */
    public function getCommit($owner, $repo, $sha){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,'https://api.github.com/repos/'.$owner.'/'.$repo.'/commits/'.$sha);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.3 Safari/605.1.15');
        curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $respuesta = curl_exec ($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close ($ch);
        if($http_status == 200){
            return json_decode($respuesta);
        }else if($http_status == 401){
            throw new \Exception('Usuario no autorizado para acceder a este repositorio.');
        }else{
            throw new \Exception('Error en la libreria.');
        }
    }
    /**
     * Metodo para obtener los branches de un repositorio.
     */
    public function getBranches($owner, $repo, $pagina=1){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,'https://api.github.com/repos/'.$owner.'/'.$repo.'/branches?page='.$pagina);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.3 Safari/605.1.15');
        curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $respuesta = curl_exec ($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close ($ch);
        if($http_status == 200){
            return json_decode($respuesta);
        }else if($http_status == 401){
            throw new \Exception('Usuario no autorizado para acceder a este repositorio.');
        }else{
            throw new \Exception('Error en la libreria.');
        }
    }
    /**
     * Metodo para obtener un registro de eventos en el repositorio.
     */
    public function getEventsLog($owner, $repo, $pagina=1){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,'https://api.github.com/repos/'.$owner.'/'.$repo.'/events?page='.$pagina);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.3 Safari/605.1.15');
        curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $respuesta = curl_exec ($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close ($ch);
        if($http_status == 200){
            if($http_status == 200){
            return json_decode($respuesta);
        }else if($http_status == 401){
            throw new \Exception('Usuario no autorizado para acceder a este repositorio.');
        }else{
            throw new \Exception('Error en la libreria.');
        }
        }else if($http_status == 401){
            throw new \Exception('Usuario no autorizado para acceder a este repositorio.');
        }else{
            throw new \Exception('Error en la libreria.');
        }
    }

}    